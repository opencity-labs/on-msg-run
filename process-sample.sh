#!/usr/bin/env bash
# vim: autoindent tabstop=2 shiftwidth=2 expandtab softtabstop=2 fileencoding=utf-8
#

set -e -o pipefail

[[ $DEBUG ]] && set -x

echoerr()
{
  echo -e $* > /dev/stderr
}

process_event()
{ 
  local content=$1
    
  local test=$(echo "$content" | jq -r . 2>/dev/null)
  if [[ $? -gt 0 ]] || [[ -z $test ]]; then
          echo -e "[INFO] found an invalid json in kafka message, ignoring it:\n${content}"
          exit 0
  fi

  local ev=$(echo "$content" | jq -r .event_version)
  
  if [[ -z $ev ]] || [[ $ev == null ]]; then
    echoerr "[ERROR] missing or incorrect version, cannot continue:\n${content}"
    exit 2 
  fi
  
  case $ev in
  
    1)
      echo "[INFO] Received event v1"
    ;;
   
    *)
      [[ $VERBOSE ]] && echo "[DEBUG] Ignored event v${ev}"
    ;;
  
  esac
}


read
in="$REPLY"
while IFS= read line; do
    in="$in $line"
done < /dev/stdin

process_event "$in"
