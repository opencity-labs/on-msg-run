#!/usr/bin/env bash
# vim: autoindent tabstop=2 shiftwidth=2 expandtab softtabstop=2 fileencoding=utf-8
#

[[ $DEBUG ]] && set -x
if [[ -z $PB_BASENAME ]] || [[ -z $PB_USERNAME ]] || [[ -z $PB_PASSWORD ]]; then
  echoerr "Missing some required variable PB_BASENAME, PB_USERNAME or PB_PASSWORD"
  exit 9
fi
stack=${PB_STACK:?"Missing required paramter: PB_STACK"}

echoerr()
{
  echo $* 1>&2
}

pb_init()
{
  pb_token=$(http --check-status --ignore-stdin POST $PB_BASENAME/collections/users/auth-with-password identity=$PB_USERNAME password=$PB_PASSWORD | jq -r .token)
  if [[ $? -gt 0 ]]; then
    echoerr "Cannot get pb_token"
    return 1
  fi
  echo "$pb_token"
}

# converte l'id di un tenant che arriva nei nostri eventi nel tenant slug che serve alla 
# console di sf per trovare il tenant corretto.
tenant_id_to_slug()
{
  local id=${1:?'required parameter tenant_id'}
  local identifier=''

  # check if in cache
  if [[ -f /tmp/tenants.cache.txt ]]; then
    identifier=$(grep "^${id}:" /tmp/tenants.cache.txt | cut -d: -f2)
    if [[ -n $identifier ]]; then
      # cache HIT
      echo $identifier
      return
    fi
  fi
  # cache MISS

  local pb_result
  local result_count
  local pb_token=$(pb_init)
  local filter="%28uuid%3D%27${id}%27+%26%26+stack%3D%27${stack}%27%29"
  # check in pocketbase
  if [[ -n $pb_token ]]; then
    pb_result=$(http --check-status --ignore-stdin "$PB_BASENAME/collections/sdc_tenants/records?filter=${filter}" Authorization:$pb_token)
    if [[ $? -gt 0 ]]; then
      echoerr "Call to '$PB_BASENAME' failed: $pb_result"
      return
    fi

    result_count=$(echo $pb_result | jq -r .totalItems)
    # [[ $VERBOSE ]] && echoerr "$result_count risultato/i in pocketbase"
    if [[ $result_count == "1" ]]; then
      identifier=$(echo $pb_result | jq -r .items[0].identifier)
      [[ $VERBOSE ]] && echoerr "identifier=$identifier"
    fi
    
  fi

  if [[ -n $identifier ]]; then
    # non ho fatto un HIT, quindi aggiorno la cache
    echo "${id}:${identifier}" >> /tmp/tenants.cache.txt
    echo $identifier
  fi
}



process_event()
{ 
  local payment_info="$1"
  local sdc_console="./bin/console"
    
  local test=$(echo "$payment_info" | jq -r . 2>/dev/null)
  if [[ $? -gt 0 ]]; then
          echo -e "[INFO] found an invalid json in kafka message, ignoring it:\n${payment_info}"
          return
  fi
  
  local event_id=$(echo $payment_info | jq -r ".event_id")
  local event_version=$(echo $payment_info | jq -r ".event_version")
  local event_created_at=$(echo $payment_info | jq -r ".event_created_at")
  local payment_id=$(echo $payment_info | jq -r ".id")
  local tenant_id=$(echo $payment_info | jq -r '.tenant_id')
  local status_name=$(echo $payment_info | jq -r '.status')
  local application_id=$(echo $payment_info | jq -r '.remote_id')
  
  [[ -z $event_id ]] && echoerr "[ERROR] missing event_id, cannot continue: '${payment_info}'" && return
  [[ -z $event_version ]] && echoerr "[ERROR] missing event_version, cannot continue" && return
  [[ -z $event_created_at ]] && echoerr "[ERROR] missing event_created_at, cannot continue" && return
  [[ -z $payment_id ]] && echoerr "[ERROR] missing payment_id, cannot continue" && return
  [[ -z $tenant_id ]] && echoerr "[ERROR] missing tenant_id, cannot continue" && return
  [[ -z $status_name ]] && echoerr "[ERROR] missing status_name, cannot continue" && return
  [[ -z $application_id ]] && echoerr "[ERROR] missing application_id, cannot continue" && return
  
  [[ $DEBUG ]] && echo "[DEBUG] payment_id=$payment_id, tenant_id=$tenant_id, status_name=$status_name, application_id=$application_id"
  
  if [[ $event_version != "1.0" ]]; then
  
    # mostra un warning la prima volta che viene incontrato un evento di una versione non supportata, poi diventa silenzioso a meno che non ci sia VERBOSE
    if [[ -f /tmp/version_warning ]]; then
      [[ $VERBOSE ]] && echo "[DEBUG] ${event_id} (v${event_version}) @${event_created_at}: ignored, this service manage only payment event's version 1.0"
    else
      echo "[WARN] ${event_id} (v${event_version}) @${event_created_at}: ignored, this service manage only payment event's version 1.0 - this alert is logged just once, following events with unsupported versions will not produce any logs unless VERBOSE mode is enabled"
      touch /tmp/version_warning
    fi
  
    return
  fi
  
  local tenant_slug=$(tenant_id_to_slug $tenant_id)
  
  # se non trovo il tenant_slug potrebbe essere perché sto vedendo messaggi di un altro env
  # o perche' effettivamente i messaggi sono di un tenant non configurato: in quel caso
  # mando la prima volta un mesaggio a livello INFO, poi lo ignoro in modo silenzionso
  if [[ -z $tenant_slug ]]; then
    if [[ -f "/tmp/ignoring-${tenant_id}" ]]; then
      [[ $VERBOSE ]] && echo "[DEBUG] ${event_id} (v${event_version}) @${event_created_at}: ignored, tenant missing in this script, if needed please add tenant '$tenant_id' to tenants array"
    else
      touch "/tmp/ignoring-${tenant_id}"
      echo "[INFO] ${event_id} (v${event_version}) @${event_created_at}: ignored, tenant missing in this script, if needed please add tenant '$tenant_id' to tenants array"
    fi
  
    return
  fi
  
  # ancora piu' debuggoso... ma non serve +
  # [[ $VERBOSE ]] && echo "[DEBUG] ${event_id} (v${event_version}) @${event_created_at}: payment '$payment_id' in status ${status_name} for '$application_id' of tenant '$tenant_id' ($tenant_slug)"
  
  case $status_name in
  
    PAYMENT_STARTED)
      # pratica pagata ma ancora non abbiamo riscontro dal sistema di pagamento
      # la pratica restera' in questo stato per potenzialmente molto tempo, il poller chiede info e ogni volta
      # il proxy emette evento di aggiornamento. Noi vogliamo cambiare stato il meno possibile, magari 1 volta soltanto
      # mettiamo un temp file che ci dice che abbiamo già fatto il cambio
      # se il servizio viene riavviato verrà fatto un'altra volta
      if [[ ! -f "/tmp/${payment_id}" ]]; then
  
        $sdc_console ocsdc:application:change-status --instance ${tenant_slug} --id $application_id --status 1510 -vvv
        if [[ $? -gt 0 ]]; then
            echo "[ERROR] ${event_id} (v${event_version}) @${event_created_at}: error setting application '$application_id' to 1510 - outcome pending - tenant '$tenant_id' ($tenant_slug)"
        else
            echo "[INFO] ${event_id} (v${event_version}) @${event_created_at}: successfully set application '$application_id' to 1510 - outcome pending - tenant '$tenant_id' ($tenant_slug)"
        fi
        touch /tmp/$payment_id
  
      else
  
        [[ $VERBOSE ]] && echo "[DEBUG] ${event_id} (v${event_version}) @${event_created_at}: '$application_id' already set to 1510, nothing to do - tenant '$tenant_id' ($tenant_slug)"
  
      fi
    ;;
  
    COMPLETE)
      $sdc_console ocsdc:application:change-status --instance ${tenant_slug} --id $application_id --status 1520
      if [[ $? -gt 0 ]]; then
         echo "[ERROR] ${event_id} (v${event_version}) @${event_created_at}: error setting application '$application_id' to 1520 - payment complete - tenant '$tenant_id' ($tenant_slug)"
      else
         echo "[INFO] ${event_id} (v${event_version}) @${event_created_at}: successfully set application '$application_id' to 1520 - payment complete - tenant '$tenant_id' ($tenant_slug)"
      fi
    ;;
  
    PAYMENT_FAILED)
      $sdc_console ocsdc:application:change-status --instance $tenant_slug --id $application_id --status 1530
      if [[ $? -gt 0 ]]; then
         echo "[ERROR] ${event_id} (v${event_version}) @${event_created_at}: error setting application '$application_id' to 1530 - payment failed - tenant '$tenant_id' ($tenant_slug)"
      else
         echo "[INFO] ${event_id} (v${event_version}) @${event_created_at}: successfully set application '$application_id' to 1530 - payment failed - tenant '$tenant_id' ($tenant_slug)"
      fi
    ;;
  
    *)
      [[ $VERBOSE ]] && echo "[DEBUG] ${event_id} (v${event_version}) @${event_created_at}: nothing to do on payment '$payment_id' in status '$status_name' in application '$application_id' of tenant $tenant_id ($tenant_slug)"
    ;;
  
  esac
}


read
in="$REPLY"
while IFS= read line; do
    in="$in $line"
done < /dev/stdin

process_event "$in"
