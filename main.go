package main

import (
	"bytes"
	"context"
	"github.com/segmentio/kafka-go"
	"log"
	"strconv"
	"time"
	"os"
	"os/exec"
	"strings"
)

const ShellToUse = "bash"

var myconfig kafka.ReaderConfig

var version string


func getFromEnv(name string, defaultValue string) string {

	if name == "" {
		return ""
	}

	val, ok := os.LookupEnv(name)
	if !ok {
		if defaultValue == "" {
			return ""
		} else {
			//log.Printf("Variable %q not defined, returned default value %q", name, defaultValue)
			return defaultValue
		}
	}
	//log.Printf("Variable %q found in environment, returned value %q", name, val)	
	return val
}

func getIntFromEnv(name string, defaultValue int) int {

	stringVal := getFromEnv(name, strconv.Itoa(defaultValue))
	intVal, err := strconv.Atoi(stringVal)
	if err != nil {
		log.Fatal("Error converting parameter %q to int", name)
	}
	return intVal
}

func Shellout(command string, eventValue []byte) ([]byte, []byte, error) {

	cmdAndArgs := strings.Split(command, " ")
	cmd := exec.Command(cmdAndArgs[0], cmdAndArgs[1:]...)

    //log.Printf("shellout %q", command)
	cmd.Stdin = bytes.NewReader(eventValue)

	var outb, errb bytes.Buffer
	cmd.Stdout = &outb
	cmd.Stderr = &errb

	err := cmd.Run()
    return outb.Bytes(), errb.Bytes(), err
}

func main() {

	InfoLog := log.New(os.Stdout, "[KRUN] INFO ", log.Ldate|log.Ltime|log.Lshortfile)
	ErrorLog := log.New(os.Stderr, "[KRUN] ERROR ", log.Ldate|log.Ltime|log.Lshortfile)
	DebugLog := log.New(os.Stdout, "[KRUN] DEBUG ", log.Ldate|log.Ltime)
	CmdOutLog := log.New(os.Stdout, "", 0)
	CmdErrLog := log.New(os.Stderr, "", 0)

	KafkaServer := getFromEnv("KAFKA_SERVER", "kafka:9092")
	KafkaTopic := getFromEnv("KAFKA_TOPIC", "test")
	ConsumerGroup := getFromEnv("KAFKA_CONSUMER_GROUP", "")
	TopicOffset := int64(getIntFromEnv("KAFKA_TOPIC_OFFSET", 0))
	TopicPartition := getIntFromEnv("KAFKA_TOPIC_PARTITION", 0)
	command := getFromEnv("COMMAND", "jq .")

	InfoLog.Printf("Starting KRUN %s", version)
	InfoLog.Printf("For each message received the message value will be passed as stdin to the command %q", command)

	if ConsumerGroup == "" {

		// No consumer group
		InfoLog.Printf("Connecting to kafka server %q without a consumer group to topic '%s:%d', running %q on each message\n", KafkaServer, ConsumerGroup, KafkaTopic, TopicOffset, command)

		myconfig = kafka.ReaderConfig {
			Brokers:				[]string{KafkaServer},
			Topic:					KafkaTopic,
			MaxWait:				30 * time.Second,
			Partition:				TopicPartition,
			PartitionWatchInterval: 5 * time.Second,
			StartOffset:            TopicOffset,
			ReadBackoffMax:         86400 * 10 * time.Second,
			Logger:                 DebugLog,
		}
	} else {

		// Consuming as a consumer group
		InfoLog.Printf("Connecting to kafka server %q as %q to topic '%s:%d', running %q on each message\n", KafkaServer, ConsumerGroup, KafkaTopic, TopicOffset, command)

		myconfig = kafka.ReaderConfig {
			Brokers:				[]string{KafkaServer},
			Topic:					KafkaTopic,
			GroupID:				ConsumerGroup,
			MaxWait:				30 * time.Second,
			PartitionWatchInterval: 5 * time.Second,
			// se metto questo mi riparte sempre dall'inizio
			// StartOffset:            kafka.LastOffset,
			ReadBackoffMax:         86400 * 10 * time.Second,
			Logger:                 DebugLog,
		}
	}
	r := kafka.NewReader(myconfig)

	if ConsumerGroup == "" {
		r.SetOffset(TopicOffset) // if 0 start from the beginning
		InfoLog.Printf("Setting topic %q offset to %d\n", KafkaTopic, TopicOffset)
	}

	defer func() {
		// chiude il reader di kafka
		err := r.Close()
		if err != nil {
			ErrorLog.Println("Error closing consumer: ", err)
			return
		}
		InfoLog.Println("Consumer closed successfully during exit routine.")
	}()

	ctx := context.Background()

	// for each message on the topic a command is executed
	for {
		m, err := r.ReadMessage(ctx) // read without committing
		if err != nil {
			ErrorLog.Println("error reading message from kafka:", err)
			continue
		}

		// the message is passed to the external command as stdin
		stdout, stderr, err := Shellout(command, m.Value)
		if err != nil {
			ErrorLog.Printf("FAILED[%d:%d]: key=%q err=%q\n", m.Partition, m.Offset, string(m.Key), err)
		    //event := bytes.NewBuffer(m.Value).String()
			//DebugLog.Printf("FAILED[%d:%d]: event\n%s", m.Partition, m.Offset, event)
		} else {
			InfoLog.Printf("SUCCESS[%d:%d]: key=%q\n", m.Partition, m.Offset, string(m.Key))
		}
		if len(strings.TrimSpace(string(stdout))) > 0 {
			CmdOutLog.Printf("%s", stdout)
		}
		if len(strings.TrimSpace(string(stderr))) > 0 {
			CmdErrLog.Printf("%s", stderr)
		}

		// autocommit if in a consumergroup
		if (ConsumerGroup != "") && (TopicOffset != 0) {
			if err := r.CommitMessages(ctx, m); err != nil {
				ErrorLog.Fatal("failed to commit message:", err)
			}
		}
	}
}
