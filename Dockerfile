FROM golang:1.19-alpine as builder
RUN apk --no-cache add ca-certificates gcc git httpie musl-dev
WORKDIR /build

# Fetch dependencies
COPY go.mod ./
RUN go mod download

# Build
COPY . ./

ARG VERSION=unknown
RUN ./build.sh

# Create final image
FROM alpine
RUN apk --no-cache add bash httpie jq
WORKDIR /
COPY --from=builder /build/krun .
CMD ["./krun"]
