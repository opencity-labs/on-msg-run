 Krun 

## Description

Esecute a system command on a kafka message.

Krun does not perform any validation on the message

## Install

* Download binary
* [Run as Docker container](https://gitlab.com/opencity-labs/krun/container_registry)

## Usage

Krun is configured using environment variables

| Variable | Required | Default value | Description    |
|----------|----------|---------------|----------------|
| KAFKA_TOPIC | Yes   | -             | The topic to read messages from
| KAFKA_SERVER | Yes  | -             | Comma-separated list of Kafka Servers
| KAFKA_CONSUMER_GROUP | No | krun          | The Consumer Group Name
| COMMAND  | Yes      | -             | Command executed on each event: the command receive the Event from stdin |
| VERBOSE  | No       | null          | Increase output verbosity
| DEBUG    | No       | null          | Ultra-detailed output


## Test

`$ docker-compose up -d`

`$ docker-compose up --build krun`

`$ docker-compose up init`

## Contributing

Tests and documentations are the most needed

Remember to run `go mod tidy` to update mod.go & mod.sum in case of new depencendies

## Authors and acknowledgment

* Opencity Labs
* Basic code from ChatGPT
* Thanks to [logocomune](https://github.com/logocomune) for the help in managing stdin

## License

GPL 3.0 only

## Active development

Don't use if you don't know exactly what you are doing.
