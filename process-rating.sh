#!/usr/bin/env bash
# vim: autoindent tabstop=2 shiftwidth=2 expandtab softtabstop=2 fileencoding=utf-8
#
# Configurare i tenant SOLO dell'ambiente corretto nella funzione tenant_id_to_slug
#

[[ $DEBUG ]] && set -x

echoerr()
{
  echo $*
}

process_event()
{ 
  local content="$1"
    
  local test=$(echo "$content" | jq -r . 2>/dev/null)
  if [[ $? -gt 0 ]]; then
          echo -e "[INFO] found an invalid json in kafka message, ignoring it:\n${content}"
          return
  fi
 

  content=$( echo $content | sed 's/widget-version/widget_version/')
  content=$( echo $content | sed 's/entrypoint-id/entrypoint_id/')

  local version=$(echo $content | jq -r ".version")
  local widget_version=$(echo $content | jq -r ".widget_version")
  local entrypoint_id=$(echo $content | jq -r ".entrypoint_id")
  
  [[ -z $version ]] && echoerr "[ERROR] missing version, cannot continue:\n${content}" && return
  [[ -z $widget_version ]] && echoerr "[ERROR] missing widget-version, cannot continue:\n${content}" && return
  
  [[ $DEBUG ]] && echo "version=$version widget-version=$widget_version"
  case $status_name in
  
    13e340f7-66b5-4a86-b2e8-81e0ad07f396)
      echo "matched event of 13e340f7-66b5-4a86-b2e8-81e0ad07f396"
    ;;
   
    *)
      [[ $VERBOSE ]] && echo "[DEBUG] nothing to do"
    ;;
  
  esac
}

in=""
while read line; do
    in="$in $line"
done < /dev/stdin

process_event "$in"
